Kiley LeClair
Assignment 3
200326066


Preprocessing done on word list


Solution to the first part is to remove the trailing 's.  This is done by looking for
all lines that end in 's, and printing all lines that fail (-v flag).

grep -v ".*'s$" american-english > no-possessive-words

The second part is harder, but GNU grep has the -P flag which allows you to use 
Perl regular expressions.  We want find all lines that contain a character
not in the range 0x00 to 0x7f, and print all the lines that fail that test :

grep -P -v '[^\x00-\x7f]' no-possessive-words > no-unicode-words




This gives a 1350 page list of words(72875)




using:
grep -P -v '[^\x00-\x6f]' no-unicode-words > no-anything-no-more

narrows the word list to 84 pages( 4514 words)

This is not the best way to decrease the word list however it works with keeping some degree of randomness





Files:

main:
- Uses a size 15 Char array to get a word from a file.
- This file right now is called no-anything-no-more.txt
- it opens this file using fopen
- it runs through 5 iterations(1 per word)
- it generates a random number using rand() and sran() for time
- it iterates through the list in a while loop until it finds the random number
- once it finds the word it counts the size
- The size of the word is used to help make an off set. This is because the final result is stored into one 
  large array of all the 5 words. 
- The large array of all the words is then printed

notes:
- There are a lot of commented out cout lines. I used these in testing within a Microsoft Visual studio environment.
- There is a picture taken from snoopy of me calling the size function on bitbucket



To build:

g++ ASSIGNMENT3.cpp
or 
Use the Kiel environment
